+++
title = "GitLab Content Move"
weight = 50
+++

Due to changes in the [GitLab free tier plan](https://about.gitlab.com/pricing/faq-efficient-free-tier/) we moved our group to the parent level. The new URL to the GitLab group is now [http://gitlab.com/ansible-ssa](https://gitlab.com/ansible-ssa).

You might have to update your local git clone with the following command:

```bash
# navigate to your local git repo
cd /path/to/my/code
# change upstream origin URL
git remote set-url origin git@gitlab.com:ansible-ssa/<project name>.git
# for example, the main playbook-rhaap project
git remote set-url origin git@gitlab.com:ansible-ssa/playbook-rhaap.git
# or this pages documentation project
git remote set-url origin git@gitlab.com:ansible-ssa/pages.git
```
