+++
title = "Setup infrastructure"
weight = 20
+++

## Setup automation controller credentials

The `ansible.controller` collection can use environment variables to access the API. Set them before you launch `ansible-navigator`:

```bash
export CONTROLLER_HOST=you-controller.example.com
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=yourpassword
export CONTROLLER_VERIFY_SSL=true # disable by using "false" instead
```

{{% notice note %}}
In order to run the playbook successfully, it is required to add the created host to a DNS service and define these variables.
Hence you can predict these settings, even if your controller is not yet installed.
The playbook can use dyndns to automatically add the host to your DNS service (see extra variables).
{{% /notice %}}

## Setup Provider Credentials

It is recommended to use environment variables for infrastructure credentials since this is the method automation controller uses when injecting credentials as well. You can store the variables in a text file and source it into your shell.

Choose one of the following providers if you do not bring your own VM or instance, but you want to use the playbooks provided to create an instance or VM for you. If you are a Red Hat Employee of official partner and have access to [Red Hat Product Demo](https://rhpds.redhat.com/) portal, you can request [Open Environment](https://redhat.highspot.com/items/601416c566bbaa1985ab3218) credentials.

{{% notice note %}}
There are two variables: `type` is used to choose where you want to deploy the automation controller instance. `controller_enable_provider` is used to enable and configure the dynamic inventories. The dynamic inventory for provider specified in 'type' will automatically be enabled. For example if you use `type == ec2` to deploy on Amazon EC2, AWS will automatically configured as a dynamic inventory in automation controller, even if you didn't set `controller_enable_provider_ec2 == true`.
{{% /notice %}}

{{% notice warning %}}
The playbooks and roles do not handle your DNS setup! If you want to use nice FQDN's (which is highly recommended), you will have to take care of managing DNS by yourself. This is mainly because there are too many ways to setup and manage DNS. If you have a good idea on how to improve this, feel free to open a Feature Request.
{{% /notice %}}

### Amazon AWS

Environment variables to use Amazon EC2:

```bash
export AWS_ACCESS_KEY_ID=<YOUR ACCESS KEY>
export AWS_SECRET_ACCESS_KEY=<YOUR SECRET KEY>
```

To deploy automation controller on Amazon AWS support set the following variable:

```yaml
type: ec2
```

To add AWS credentials and dynamic inventory set the following variable:

```yml
controller_enable_provider_ec2: true
```

Please read the [Amazon Web Services Guide](https://docs.ansible.com/ansible/latest/scenario_guides/guide_aws.html) to learn more about setting up AWS for Ansible.

### Microsoft Azure

Environment variables to use Microsoft Azure:

```bash
export AZURE_SECRET=<YOUR AZURE SECRET>
export AZURE_CLIENT_ID=<YOUR AZURE CLIENT ID>
export AZURE_SUBSCRIPTION_ID=<YOUR AZURE SUBSCRIPTION ID>
export AZURE_TENANT=<YOU AZURE TENANT ID>
```

Information on how to set this up can be found in the [Ansible Documentation](https://docs.ansible.com/ansible/latest/scenario_guides/guide_azure.html).

To enable Microsoft Azure support set the following variable:

```yaml
type: azure
```

To add Azure credentials and dynamic inventory set the following variable:

```yml
controller_enable_provider_azure: true
```

Please read the [Microsoft Azure Guide](https://docs.ansible.com/ansible/latest/scenario_guides/guide_azure.html) to learn more about setting up Azure for Ansible.

### Google GCP

Environment variables to use Google GCP:

```bash
export GCP_AUTH_KIND=serviceaccount
export GCP_SERVICE_ACCOUNT_FILE=~<PATH TO YOUR ACCOUNT FILE>
export GCP_PROJECT=<NAME OF YOUR GCP PROJECT>
```

To enable Google GCP support set the following variable:

```yaml
type: gcp
```

To add GCP credentials and dynamic inventory set the following variable:

```yml
controller_enable_provider_gcp: true
```

Please read the [Google Cloud Platform Guide](https://docs.ansible.com/ansible/latest/scenario_guides/guide_gce.html) to learn more about setting up GCP for Ansible.

### Source the environment variables

To read the variables in your current shell session:

```bash
source </path/to/environment/file>
```
