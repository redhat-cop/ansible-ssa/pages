+++
title = "Use Case: Automation Hub"
weight = 54
+++

To enable this use case make sure to set the following variable:

```yaml
controller_ah_enable: false
controller_ah_instance_name: hub
```

## About Automation Hub

Automation Hub has three primary use cases:

- make specific content from Ansible Galaxy available within the enterprise

- make specific certified content from Red Hat's Automation Hub available within the enterprise

- store and distribute collections created within the enterprise

### Configure Automation Hub

Right now the Playbook will deploy Red Hat Ansible Automation Platform and an instance of the private Automation Hub, The automation hub is already synchronized with your content from upstream automation hub, execution environments and some other content.

Automation controller is also already configured to retrieve content from the private automation hub.
