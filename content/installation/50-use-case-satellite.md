+++
title = "Use Case: Satellite 6"
weight = 50
+++

This is an optional use case to demonstrate Red Hat Satellite 6. To enable setting up this use case use set the boolean `use_case_satellite` to `true`.

This will:

- setup a Job Templates in automation controller to create a Satellite instance, setup and configure Satellite

- setup a Job Template to register a Satellite Server on the Red Hat Service Portal

## Variables

```yaml
# enable setting up the Satellite Use Case
use_case_satellite: true
# Satellite specific variables
# admin password
satellite_admin_password: "{{ vault_satellite_admin_password }}"
# enable RHEL 7 content, this will sync RHEL 7 Server channels, create a Content View and an RHEL7 activation key
satellite_sync_rhel7: false
# enable RHEL 8 content, this will sync RHEL 8 BaseOS and Appstream channels, create a Content View and an RHEL8 activation key
satellite_sync_rhel8: false
# if you have problems with the installer checks (e.g. DNS lookup checks), set this to true
satellite_skip_checks: false
```

## Manuel steps

Before running the Job Template to setup Satellite Server, you have to register the VM with the Red Hat Service Portal. YOu can use the provided Job Template or do it manually. Also make sure your Satellite Server does have a proper FQDN registered in DNS.

## Issues on Azure

If you deploy Satellite on Azure, there are currently some manual steps necessary:

- you will have to use `-s` to disable checks (this is done in the role by using satellite_skip_checks = true)
- add the FQDN with internal and public IP to `/etc/hosts`
- manually disable rhel-7-server-htb-rpms if enabled
- use `hostnamectl set-hostname <public FQDN>`

There is already an [open issue on GitLab](https://gitlab.com/ansible-ssa/role-satellite-setup/-/issues/1) to track this.

{{% notice warning %}}
During testing we noticed Azure phases a lot of stability issues. The same Ansible Playbook can fail several times and suddenly succeed without errors - you might even see different errors when rerunning the Job Template. It's unclear what's the root cause though.
{{% /notice %}}

## Workflow

1. setup automation controller

1. create a DNS record for Satellite

1. run the Job Template "Satellite - Deploy a new instance"

{{% notice note %}}
Make sure to choose the correct provider and instance flavor. For Azure you have to select "Standard_DS4" or Satellite will fail during the initial content sync. For Google "n1-standard-8" is the right choice.
{{% /notice %}}

1. log into the Satellite VM

    - add public and internal IP to /etc/hosts with the FQDN

    - set the hostname to the FQDN

    - use subscription-manager to register on RHN

    - attach a proper entitlement (obviously make sure the Satellite repo is included)

    - make sure rhel-7-server-htb-rpms is not enabled (`subscription-manager repos --disable=rhel-7-server-htb-rpms`)

    - either use `subscription-manager` attach or the web portal to assign the proper entitlements to the system

1. copy your Satellite manifest to `/var/tmp/satellite-manifest.zip` on the automation controller (the playbook will pick it up from there and import into Satellite)

1. run the Job Template "Satellite - Install on existing instance", make sure to set the correct `limit` and enable RHEL7 or RHEL8 content. Make also sure to set `satellite_skip_checks: true` when deploying on Azure or Google (this should be automatically set for you).
￼
1. run the Job Template "Satellite - Configure Content" to finish the configuration
