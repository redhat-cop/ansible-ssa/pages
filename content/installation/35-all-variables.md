+++
title = "All variables"
weight = 35
+++

This page contains an automatically generated list of all default variables for each role. Unless labeled **mandatory**, there are reasonable defaults for each variable as documented below. In most cases, overriding the defaults is not necessary.

Make sure you add the variables which are flagged mandatory to your variable file prior to the playbook run.
