+++
title = "Containerized Installation"
weight = 45
+++
**NOTE:** This project does not contain the installer files, you have to download them manually.

In the Red Hat Ansible Automation Platform 2.4 a new installation method was introduced. This installation method is using containers with podman underneath, but does not require a container platform like OpenShift. Learn more about this new installation method in the blog post [Announcing Containerized Ansible Automation Platform](https://www.redhat.com/en/blog/announcing-containerized-ansible-automation-platform).

This new installation method is particularly useful for demos, testing and development because you can install everything inside a virtual machine on your laptop or workstation. Note that with Ansible Automation Platform 2.4 the containerized installation is still [Technology Preview](https://docs.redhat.com/en/documentation/red_hat_ansible_automation_platform/2.4/html/containerized_ansible_automation_platform_installation_guide/aap-containerized-installation).

To use containerized AAP with this demo project, you need a virtual machine or cloud instance with Red Hat Enterprise Linux 9.4 or later installed. The provided playbook can create a cloud instance on AWS, Azure or Google Cloud, but not local virtual machines (like KVM, VMware Workstation, VirtualBox). Obviously KVM is highly recommended - if you need a UI, check out [virt-manager](https://virt-manager.org/).

## Prerequisites

Make sure you followed the general instructions to [setup your workstation](../10-setup-workstation/).

Clone the Playbook CAAP project to your local machine:

```bash
git clone https://gitlab.com/ansible-ssa/playbook-caap.git
```

If you want to use the capability to create a instance on one of the supported providers, create the necessary environment variables described in the chapter [setup infrastructure](../20-setup-infrastructure/). Depending on your cloud provider or virtualization choice, you might have to set [extra variables](../30-extra-variables/) in the file below as well.

Create a variable file with the following content. This is only the minimal list of variables that are required, optional variables can be set as well. A full list can be found in chapter [all variables](../35-all-variables/).

```yaml
---
caap_fqdn: <FQDN or IP of the virtual machine or instance>
# look for ansible-automation-platform-containerized-setup in /tmp (OPTIONAL)
# caap_install_dir: /tmp
controller_admin_password: "{{ vault_controller_admin_password }}"
# do not deploy Let's encrypt certificates
# set this to false if you have DynDNS set up
letsencrypt_skip: true
letsencrypt_staging: true
quay_registry_username: <your user name>
quay_registry_password: "{{ vault_quay_registry_password }}"
rhsm_username: <your user name>
rhsm_password: "{{ vault_rhsm_password }}"
rhaap_manifest: '{{ lookup("file","/path/to/your/manifest.zip") }}'
```

{{% notice note %}}
In the example above all variables starting with `vault_` are stored in an encrypted vault file and not in clear text.
{{% /notice %}}

### Environment variables

Your environment file (for example save this as `environment-variables.sh`) for the cloud credentials and your controller should look something like this:

```bash
# if you use AWS
export AWS_ACCESS_KEY_ID=<access key>
export AWS_SECRET_ACCESS_KEY=<secret key>
# if you use Azure
export AZURE_SECRET=<azure password>
export AZURE_CLIENT_ID=<azure client ID>
export AZURE_SUBSCRIPTION_ID=<azure subscription ID>
export AZURE_TENANT=<azure tenant name>
# if you use Google Compute Cloud
export GCP_AUTH_KIND=serviceaccount
export GCP_PROJECT=<your project name>
export GCP_SERVICE_ACCOUNT_FILE=~/.gcp.json
# these are the credentials to automate your controller
export CONTROLLER_HOST=https://<FQDN or IP>:8443
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=<your secret password>
export CONTROLLER_VERIFY_SSL=false
```

It's best to save all this in a file to make it easier to set them.

### Provide setup archive

You have to download the latest version of the Red Hat Ansible Automation Platform setup tar ball from [access.redhat.com](https://access.redhat.com). If you store the file in the same directory where you cloned the git repository to, the Playbook will find it automatically. Otherwise, you can set the variable `caap_install_dir` and the Playbook will search for the file there.

Check the current value of the `caap_version` variable, this is the version this code is currently tested with. You can find the current value in the [configuration file](https://gitlab.com/ansible-ssa/role-caap-setup/-/raw/main/defaults/main.yml?ref_type=heads). If you want to test a different version, make sure to set the `caap_version` correctly. For example, if the archive is called `ansible-automation-platform-containerized-setup-2.5-1.tar.gz` the value of `caap_version` is `2.5-1`.

### DynDNS and let's encrypt

If you followed the instructions for setting up dynamic DNS in the chapter [setup infrastructure](../20-setup-infrastrcuture), you can let this Playbook configure proper let's encrypt certificates for you. Otherwise make sure to set `letsencrypt_skip` and `letsencrypt_staging` both to true.

## Perform full installation

If you want to use an existing virtual machine or cloud instance, you can skip forward to [configure existing system](#configure-existing-system).

After creating the variable file, make sure you load the environment variables before running the Playbook.

```bash
# source environment variables
. /path/to/your/environment-variables.sh
# Log into Quay
podman login quay.io -u <username>
ansible-navigator run caap-install.yml -e @/path/to/your/variables
```

{{% notice warning %}}
Double check you did not forget to define the cloud provider specific [extra variables](../30-extra-variables/).
{{% /notice %}}

## Configure existing system

If you already have an existing installation of the Red Hat Ansible Automation Platform, you can skip forward to [applying the post installation configuration](#apply-post-installation-configuration).

Create an inventory file with your CAAP instance and optional access parameters:

```ini
[controller_group]
# example with short host name and default SSH user configuration
caap
# example with FQDN
caap.example.com
# example with IP and user name
caap ansible_host=192.168.122.1 ansible_user=ansible
```

```bash
# caap-setup.yml Playbook to configure existing system
. /path/to/environment-variables.sh
ansible-navigator run caap-setup.yml -e @/path/to/your/variables -i inventory
```

## Apply post installation configuration

If you already have a running installation of the Red Hat Ansible Automation Platform, you can skip the previous steps and only apply the post installation configuration. You still have to configure the variables as described before.

This can also be helpful to refresh the configuration of an existing installation.

{{% notice note %}}
Be aware of the common challenges when updating your existing installation though. For example, although survey fields that have changed, will be updated. However, survey fields that are obsolete will not be removed. In some cases removing all content might give more predictable results - or consider reinstalling completely. All your configuration is done as code as well, right?
{{% /notice %}}

```bash
# caap-postinstall.yml Playbook to apply platform configuration
. /path/to/environment-variables.sh
ansible-navigator run caap-postinstall.yml -e @/path/to/your/variables -i inventory
```

{{% notice info %}}
Although this will apply configuration for private automation hub to synchronize collections from upstream automation hub, there is no sync triggered. If you want Collections and Namespaces to be synchronized, click **Sync repository** next to **Repositories** in the **Automation Content** menu. Synchronizing content can take a long time and consumes significant CPU and RAM resources, therefore automatic sync during configuration was intentionally disabled. You also probably only want to synchronize one repository at a time to avoid out of memory errors, or increase the RAM for your CAAP instance.
{{% /notice %}}
