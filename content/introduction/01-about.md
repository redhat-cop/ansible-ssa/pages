+++
title = "About this project"
weight = 1
+++

There is a lot of Ansible content available on the internet to get you started. We feel what's sometimes missing are examples on how to go from beginner to guru. The idea of this project is to provide content to go beyond the typical "Ansible Getting Started" experience and to give you some real world experience.

## Mission statement

1. Provide content that actually works in real life scenarios

1. Follow [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)

1. Make it modular as much as possible and where applicable

1. Provide explanations about the value of a particular solution, but skip the basics

We're always open for suggestions. Open an issue in GitLab and have a look at our [Developer Guide](../../contribute/)
