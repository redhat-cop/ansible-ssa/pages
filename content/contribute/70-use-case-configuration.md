+++
title = "Use Case configuration"
weight = 70
+++

Before adding a new use case, make yourself familiar with the [post installation](../60-post-install/) process.

For new use cases create a new project following the naming convention `use-case-XYZ`. Make sure to update the `configuration-controller` to create a job template for the new use case.

The use case project should contain the configuration in the format and structure recognized and supported by the `infra.controller_configuration` collection. Update the README.md with all necessary instructions to allow others to demonstrate the use case and any prerequisites it may have.

Typically you can add or update your use case content in automation controller, by relaunching the respective job template. or using the following commands:

```bash
# set environment variables
. /path/to/config
ansible-navigator run infra.controller_configuration.configure_controller -i /path/to/inventory -e @/path/to/variables -e controller_configs_dir=`pwd`/aap_configuration
```

As a minimum, the environment variables have to set the values to connect automation controller:

```bash
export CONTROLLER_HOST=http://you-controller.example.com:8443
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=yourpassword
export CONTROLLER_VERIFY_SSL=true # disable by using "false" instead
```
