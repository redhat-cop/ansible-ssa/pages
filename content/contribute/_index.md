+++
title = "Contribution"
weight = 90
chapter = true
+++

# Development and contribution of content

This chapter contains recommendations on how to create content in this project.
