+++
title = "CI Pipeline"
weight = 50
+++

To increase code quality of the playbooks, roles and modules, the project is using an extensive CI pipeline for automated testing. In a nutshell we do the following tests:

- run ansible-lint on each playbook, role and the collection

- build the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection/)

- build the [execution environment](https://gitlab.com/ansible-ssa/ee-ansible-ssa/)

- publish the execution environment on [Quay](https://quay.io/repository/redhat_emp1/ee-ansible-ssa) (access to this repository requires a Red Hat Employee account)

- run a job on automation controller which deploys AAP on AWS, Azure and Google

- notify all relevant activities on [Slack](https://slack.com)

- update the [pages](https://gitlab.com/ansible-ssa/pages/) documentation which is automatically published on [Ansible SSA Labs](https://www.ansible-labs.de) website

## GitLab CI pipeline

Each repository has a CI pipeline to triggers the following chain.

{{<mermaid align="left">}}
graph LR;
    subgraph GitLab
    role-instance(role-instance) --> collection_general(ansible_ssa.general)
    collection_general --> ee(ee-ansible-ssa)
    role-controller-content( role-controller-content ) --> collection_general
    role-controller-setup( role-controller-setup ) --> collection_general
    role-automationhub-content( role-automationhub-content ) --> collection_general
    role-automationhub-setup( role-automationhub-setup ) --> collection_general
    playbook-rhaap( playbook-rhaap ) --> collection_general
    role-win_* --> collection_windows(ansible_ssa.windows)
    collection_windows --> ee
    end

    subgraph CI AAP
    aap-ci( AAP CI Workflow ) --> ci-aws( CI AWS )
    aap-ci --> ci-azure( CI Azure )
    aap-ci --> ci-gcp( CI GCP )
    end

    subgraph Documentation
    role-instance --> pages(Pages Documentation Project)
    collection_general --> pages
    collection_windows --> pages
    role-controller-content --> pages
    role-controller-setup --> pages
    role-automationhub-content --> pages
    role-automationhub-setup --> pages
    playbook-rhaap --> pages
    end

    ee --> aap-ci
{{< /mermaid >}}

## CI AAP

There is an installation of Red Hat Ansible Automation Platform which is called to launch a CI workflow. The Workflow will run the [playbook-rhaap](https://gitlab.com/ansible-ssa/playbook-rhaap/-/blob/main/rhaap-install.yml) on AWS, Azure and Google Cloud in parallel. After each `playbook-rhaap.yml` run, the cloud resources will be removed by launching the playbook with the **remove=true** extra variable.
