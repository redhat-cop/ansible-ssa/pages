+++
title = "Postinstall configuration"
weight = 60
+++

If you perform an [installation of the containerized version oft he Red Hat Ansible Automation Platform](../../installation/45-caap-install/), some basic configuration is automatically applied during the post installation phase. This imports a user provided manifest to activate automation controller, set some base settings and imports a selection of job templates. Each job template represents a use case which can be imported separately. How co create and configure use cases is described in the section [use case configuration](../70-use-case-configuration/).

This separation into base configuration and optional use cases should make it easier to deploy, update and maintain use cases individually. This post installation configuration should therefore only apply settings which are needed for all use cases and create job templates for each use case.

The post installation configuration is automatically called because the CAAP installation is performed with the follow relevant entries in the inventory file:

```ini
# To use the postinstall feature you need to set these variables
controller_postinstall=true
controller_license_file=/tmp/manifest.zip
controller_postinstall_dir=/home/ansible/aap_configuration
# When using config-as-code in a git repository
controller_postinstall_repo_url=https://gitlab.com/ansible-ssa/configuration-controller.git
#controller_postinstall_repo_ref=main
```

There is also a section for private automation hub:

```ini
hub_postinstall=true
hub_postinstall_dir=/home/ansible/pah_configuration
# When using config-as-code in a git repository
hub_postinstall_repo_url=https://gitlab.com/ansible-ssa/configuration-automationhub.git
#hub_postinstall_repo_ref=main
```

To make changes to the post installation configuration, create issues and merge requests to the [configuration-controller](https://gitlab.com/ansible-ssa/configuration-controller) or [configuration-automationhub](https://gitlab.com/ansible-ssa/configuration-automationhub) project.

For adding a new use case it should only be necessary to add the project to the `aap_configuration/Default/projects.yml` configuration file and to add a job template to `aap_configuration/Default/job_templates.yml`.

If the new use case requires `extra_variables` you can add them to the [role-caap-setup](https://gitlab.com/ansible-ssa/role-caap-setup) - but make sure to define them as optional variables (with default('omit')).

## Testing

You can test your modifications by using the `infra.controller_configuration.configure_controller` playbook provided by the `infra.controller_configuration` collection:

```bash
# set environment variables
. /path/to/config
ansible-navigator run infra.controller_configuration.configure_controller -i /path/to/inventory -e @/path/to/variables -e controller_configs_dir=`pwd`/aap_configuration
```

As a minimum, the environment variables have to set the values to connect automation controller:

```bash
export CONTROLLER_HOST=http://you-controller.example.com:8443
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=yourpassword
export CONTROLLER_VERIFY_SSL=true # disable by using "false" instead
```

This works similarly for the `configuration_automationhub`.
