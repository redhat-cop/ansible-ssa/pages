+++
title = "Ansible SSA Team"
weight = 15
chapter = true
+++

# EMEA Ansible SSA Team

On this web page you will find several examples on how to use Ansible and Red Hat Ansible Automation Platform.

***Right now this is under heavy development and work in progress.***

***The GitLab content has moved! Read the [instructions](./gitlab-repo/) on how to update your local clone.***
